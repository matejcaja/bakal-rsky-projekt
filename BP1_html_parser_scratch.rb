#  untitled.rb
#  
#  Copyright 2013 Matej Čaja <matejcaja@gmail.com>
#  

#http://dl.acm.org/citation.cfm?id=1919
#db.execute( "INSERT INTO Products ( stockID, Name ) VALUES ( ?, ? )", [id, name])

require 'rubygems'
require 'nokogiri'
require 'open-uri'
require 'csv'

id= 1919
max= 1930

CSV.open("test.csv", "wb") do |row|
	row << ["citation_publisher","citation_journal_title","citation_title","citation_authors","citation_date","citation_abstract_html_url","citation_keywords"]
end
while id < max do
	url = "http://dl.acm.org/citation.cfm?id=#{id.to_s}"
	#url="http://dl.acm.org/citation.cfm?id=1198296.1198301&coll=DL&dl=GUIDE&preflayout=tabs"
    puts url
    
	page = Nokogiri::HTML(open(url))   

	pokus = Hash.new
	
	page.xpath('//head/meta').each do |node|
		pokus[node["name"]] = node["content"]
	end	

if !pokus['citation_title'].nil?
	if pokus['citation_keywords'].nil?
		pokus['citation_keywords'] = ""
		#pokus['tags'] = ""
		#puts pokus['citation_keywords']
		page.xpath('//*[@id="divtags"]/a/span').each_with_index do |node,index|
			if(index != 0)
				pokus['citation_keywords'] << ";#{node.text}"
			else
				pokus['citation_keywords'] << "#{node.text}"
			end	
		end
	end
=begin	
	puts "citation_publisher: #{pokus['citation_publisher']}"
	puts "citation_journal_title: #{pokus['citation_journal_title']}"
	puts "citation_title: #{pokus['citation_title']}"
	puts "citation_authors: #{pokus['citation_authors']}"
	puts "citation_date: #{pokus['citation_date']}"
	puts "citation_abstract_html_url: #{pokus['citation_abstract_html_url']}"
	puts "citation_keywords: #{pokus['citation_keywords']}"
=end	

	#puts "citation_keywords: #{pokus['tags']}"
	
		
	CSV.open("test.csv", "a+") do |row|
			row << [pokus['citation_publisher'],pokus['citation_journal_title'],pokus['citation_title'],pokus['citation_authors'],pokus['citation_date'],pokus['citation_abstract_html_url'],pokus['citation_keywords']]	
	end
end	
	puts "Going to sleep..."
	sleep 1
	id += 1
	
end


