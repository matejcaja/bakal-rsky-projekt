class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.integer :document
      t.float :doc_rating
      t.references :user, index: true

      t.timestamps
    end
  end
end
