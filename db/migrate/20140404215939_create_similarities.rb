class CreateSimilarities < ActiveRecord::Migration
  def change
    create_table :similarities do |t|
      t.integer :doc1
      t.integer :doc2
      t.float :value

      t.timestamps
    end
  end
end
