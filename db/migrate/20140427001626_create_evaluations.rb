class CreateEvaluations < ActiveRecord::Migration
  def change
    create_table :evaluations do |t|
      t.integer :person
      t.integer :no_person

      t.timestamps
    end
  end
end
