//= require jquery
//= require jquery_ujs

if ( showOrHide === true ) {
    $( "#spinner" ).show();
} else if ( showOrHide === false ) {
    $( "#spinner" ).hide();
}