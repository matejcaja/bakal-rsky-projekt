// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//= require jquery_ujs
//= require jquery
//= require turbolinks
//= require_tree .

//po kliknuti na jeden z vygenerovanych linkov sa odosle na server, ktory link sa klikol
$(document).ready(function() {
    $("[id^=link_]").click(function(event){
        $.ajax({
           url: '/update',
           success: function(data){},
           data: {doc_id:event.target.id}
        });
    });
});

//defaultne nechaj abstract schovany
$(document).ready(function(){
    $("[id^=text_]").hide();
});

//po kliknuti zobraz zvysok abstractu
$(document).ready(function(){
    $("button").click(function(event){
        if($(this).text().trim() == 'Show more'){
            $("#text_"+event.target.id).fadeToggle(600);
            $("#short_"+event.target.id).fadeToggle(0);
            $(this).text("Show less")
        }else{
            $("#text_"+event.target.id).fadeToggle(0);
            $("#short_"+event.target.id).fadeToggle(600);
            $(this).text("Show more")
        }

    });
});


