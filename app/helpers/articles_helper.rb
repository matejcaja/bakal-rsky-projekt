module ArticlesHelper

  def check_presence(value)
    if value.present?
      value
    end
  end

  def shorten_abstract(text)
    if text.length > 400
      shorten = String.new

      (0..400).each { |i|
        shorten[i] = text[i]
      }
      shorten << "..."
    else
      return text
    end


    return shorten
  end



end
