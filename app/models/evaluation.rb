class Evaluation < ActiveRecord::Base

  def save_click(doc_id)
    evaluator = Evaluation.find(1)

    if doc_id == -1
      evaluator.update :no_person => evaluator.no_person+1
    else
      evaluator.update :person => evaluator.person+1
    end
  end
end
