class User < ActiveRecord::Base
  has_many :documents

  def create_user(id)
    unless User.exists?(id)
      User.create(:name => "User", :surname => id, :id => id, :avg_rating => 0)
    end
  end
end
