json.array!(@articles) do |article|
  json.extract! article, :title, :publisher, :conference, :date, :authors, :abstract, :tags, :classifications, :published_in
  json.url article_url(article, format: :json)
end
