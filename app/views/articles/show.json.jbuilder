json.extract! @article, :title, :publisher, :conference, :date, :authors, :abstract, :tags, :classifications, :published_in, :created_at, :updated_at
