require 'rubygems'
require 'nokogiri'
require 'open-uri'
require 'tire'

class PagesController < ApplicationController
  def home
  end

  def help
   Tire.index 'articles' do
      delete
      create
      refresh
    end

  end

  def run
    if params[:min].empty? or params[:max].empty?
      redirect_to :back
    end

    id = params[:min].to_i
    max = params[:max].to_i

    while id < max do
      HardWorker.perform_async(id)
      puts "Next id in 5 seconds".red
      sleep 5
      id += 1
    end

    Tire.index 'articles' do
      refresh
    end
   end
  end
