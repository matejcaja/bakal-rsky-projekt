class ArticlesController < ApplicationController
  include ArticlesHelper
  before_action only: [:show, :search]

  def index
  end

  def search
    @search_adjust = SearchArticle.new
    #ulozenie id pouzivatela
    session[:current_user] = params[:userid]
    user = User.new
    #vytvorenie noveho pouzivatela, ak este neexistuje
    user.create_user(params[:userid])

    if params[:q].present?
      #vyhladanie dokumentov
      @result = @search_adjust.search_articles(params[:q], (Elasticsearch::Client.new log: true))
      @result = Hashie::Mash.new @result
      #ohodnotenie dokumentov
      result_brefore_paging = @search_adjust.weighted_sum(params[:userid], @result)
      #vytvorenie zoznamu na evaluaciu
      covering = @search_adjust.evaluation(result_brefore_paging, @result)
      #strankovanie
      @articles = Kaminari.paginate_array(covering).page(params[:page]).per(10)

      render :action => "search"
    else
      render :action => "index"
    end
  end

  def update
    @search_adjust = SearchArticle.new
    evaluator = Evaluation.new
    #ulozenie kliknutia na evaluaciu do databazy
    suffix_id = params[:doc_id][13..params[:doc_id].length-1].to_i
    puts "[Info]: suffix of doc_id: #{suffix_id}"
    evaluator.save_click(suffix_id)

    if  suffix_id  != -1
      doc_id = params[:doc_id][5..11].to_i

      @user = User.find(session[:current_user])

      test = Document.find_by_sql("select user_id from documents where document = #{doc_id} and user_id = #{session[:current_user]}")

      if test.present?
        @user.documents.increment_counter :doc_rating, Document.select(:id).where(:document => doc_id)
        @user.update(:avg_rating=>@user.documents.where(:user_id => session[:current_user]).average(:doc_rating))
      else
        @user.documents.create(:document=>doc_id, :doc_rating=>1)
        @user.update(:avg_rating=>@user.documents.where(:user_id => session[:current_user]).average(:doc_rating))
      end

      #vyratanie podobnosti
      @search_adjust.compute_similarities(Document.select(:document).distinct, doc_id)

    end
  end

  def show
  end

  def new
  end

  def edit
  end

  def create
  end

  def destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:title, :publisher, :conference, :date, :authors, :abstract, :tags, :classifications, :published_in)
    end

end

