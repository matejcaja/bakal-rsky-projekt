# app/workers/hard_worker.rb
class HardWorker
  include Sidekiq::Worker

  Sidekiq::Queue['default'].limit = 1

  def perform(id)
      url_site = "http://dl.acm.org/citation.cfm?id=#{id.to_s}&preflayout=flat"
      puts url_site.blue
      url = open(url_site)
      page = Nokogiri::HTML(url)

      if !page.css('div div.medium-text').text.eql? "Unknown Item"
        site = Hash.new
        #meta tag parser
        page.xpath('//head/meta').each do |node|
          site[node["name"]] = node["content"]
        end

        #parser for tags
        tags = Array.new
        page.xpath('//div[@id="divtags"]/a/span').each do |node|
          tags << node.text
        end

        #parser for abstract
        site["abstract"] = page.xpath('//div[@class="layout"]/div[@class="flatbody"]/div[@style="display:inline"]').text

        #parser for classifications
        classification = Array.new
        classification = getClassification(page, id)
        #parser for authors
        #author_information = Array.new
        #author_information = getAuthorInformation(page, id)
        author = Array.new
        author_page = Array.new
        institution = Array.new
        author_information = Array.new

        page.xpath('//tr/td/table[2]/tr').each do |node|
          author << node.xpath('./td/a[@title="Author Profile Page"]').text
          institution << node.xpath('./td/a[@title="Institutional Profile Page"]').text

          node.xpath('./td/a[@title="Author Profile Page"]').each do |test|
            author_page << "http://dl.acm.org/#{test['href']}"
          end
        end

        0.upto(author.length-1) do |i|
          if author[i].blank? && author_page[i].blank? && institution[i].blank?
            next
          end

          author_information << {:name => author[i], :page => author_page[i], :institution => institution[i]}

        end

        #article_detail
        article_detail = Array.new
        article_detail = getArticleDetail(page, id)

        Tire.index 'articles' do
          store :id => id, :citation_conference => site['citation_conference'], :citation_publisher => site['citation_publisher'],
                :citation_title => site['citation_title'], :citation_date => site['citation_date'],
                :authors => author_information, :abstract => [:text => site['abstract'], :abstract_url => site['citation_abstract_html_url']],
                :tags => tags, :classifications => classification, :published_in=> article_detail
                #,:pdf_pages => pdf_pages
          refresh
          puts "Having a break from hard work...".blue
          sleep 2
        end
    else
        puts page.css('div div.medium-text').text.red
        puts "Empty page...".red
        sleep 3
    end
  end

  def getArticleDetail(page, id)
    #article detail
    article_detail = Array.new
    a = Array.new
    page.xpath('//table[3]/tr/td/table/tr/td/text()').each do |node|
      if node.text.blank?
        next
      end
      a << node.text.split.join(" ")
    end

    a << page.xpath('//table[3]/tr/td/table/tr/td/span[@class="small-link-text"]').text.split.join(" ")

    article_detail << {:type => a[1],
                       :conference => a[2],
                       #:article_detail => a[3]+";"+is_null(a[4]), 
                       :conference_detail => a[5]
    }

    return article_detail
  end

  def getClassification(page,id)
    classification_num = Array.new
    classification_val = Array.new
    classification = Array.new

    page.xpath('//div[@class="flatbody"]/strong').each do |node|
      classification_num << node.text
    end

    page.xpath('//div[@class="flatbody"]/a').each do |node|
      classification_val << node.text
    end

    #removing first position (invalid values)
    classification_num.shift
    classification_val.shift

    classification_num.zip(classification_val).each do |num, val|
      classification << {:id => num, :value => val}
    end

    return  classification
  end
end

