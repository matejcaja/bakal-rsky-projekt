class SearchArticle

  def search_articles(query, client)
    client.search index: 'articles',
      body: {
        from: 0,
        size: 100,
        query: {
          multi_match: {
            query: query,
            fields: ["citation_title^2", "tags", "authors.name^2"]
          }
        }
      }
  end

  #functions for personalization
  def weighted_sum(user_id, articles)
    values = Document.find_by_sql("select min(doc_rating) as min,max(doc_rating) as max from documents;")
    personalized_result = Array.new(articles.hits.hits.length) { Array.new(2) }

    articles.hits.hits.each_with_index do |article, index|
      sum = 0
      dem = 0
      rated_documents = Similarities.find_by_sql("select d.document,s.doc1,s.doc2,d.user_id,d.doc_rating,value from similarities s join documents d on d.document = s.doc2 where s.doc1 = #{article._source.id} and d.user_id = #{user_id};")
      #puts article._source.id
      (0..rated_documents.length-1).each { |i|
        normalized_rating = de_normalize_rating(rated_documents[i][:doc_rating], values[0][:min], values[0][:max])

        sum += rated_documents[i][:value]*normalized_rating
        dem += rated_documents[i][:value].abs
      }

      begin
        result = de_normalize_rating((sum/dem), values[0][:min], values[0][:max])
        personalized_result[index][1] = result
        personalized_result[index][0] = article
      rescue
        puts "[Rescued]: ZeroDivisionError"
        personalized_result[index][1] = 0
        personalized_result[index][0] = article
      end
    end

    personalized_result.sort { |a, b| b[1] <=> a[1] }
  end

  def de_normalize_rating(value, min, max)
    0.5*((value+1)*(max-min))+min
  end

  def compute_similarities(stored_documents, visited_document)
    (0..stored_documents.length-1).each { |i| #prejdem si vsetky dokumenty v databaze, a vyratam ich podobnost s nakliknutym
      next if stored_documents[i][:document] == visited_document #vynechavam sameho seba
      jaccard_index(stored_documents[i][:document], visited_document) #pocitam index
    } if stored_documents.length > 1 #iba ak sa nieco nachadza v databaze
  end

  def jaccard_index(stored_document, visited_document)
    stored_document_users = Document.select(:user_id).where(:document => stored_document)
    visited_document_users = Document.select(:user_id).where(:document => visited_document)
    values_to_compute = join_arrays(stored_document_users, visited_document_users)

    conjunction = (values_to_compute.length - values_to_compute.uniq.length) #pocet unikatnych odcitam od celeho pola, zostanu duplicity
    disjunction = values_to_compute & values_to_compute #vezmem iba prienik

    if_exists = Similarities.find_by_sql("select * from Similarities where doc1 = #{visited_document} and doc2 = #{stored_document}")
    result = (conjunction.to_f/disjunction.length.to_f)

    if result > 0
      if if_exists.present?
        if_exists[0].update(:value => result)
      else
        Similarities.create(:doc1 => visited_document, :doc2 => stored_document, :value => result)
      end
    end

  end

  def join_arrays(stored_document_users, visited_document_users)
    all_users = Array.new

    (0..stored_document_users.length-1).each { |i|
      all_users << stored_document_users[i][:user_id]
    }

    (0..visited_document_users.length-1).each { |i|
      all_users << visited_document_users[i][:user_id]
    }

    all_users
  end

  def evaluation(result_brefore_paging, result)
    original_result = Array.new(result.hits.hits.length) { Array.new(2) }
    new_result = Array.new(result.hits.hits.length*2) { Array.new(2) }

    # -1 indicates non-personalized result
    result.hits.hits.each_with_index do |article, index|
      original_result[index][1] = -1
      original_result[index][0] = article
    end

    index = 0
    begin
      (0...100).each do |i|
        new_result[index][0] = original_result[i][0]
        new_result[index][1] = original_result[i][1]
        index+=1

        new_result[index][0] = result_brefore_paging[i][0]
        new_result[index][1] = result_brefore_paging[i][1]
        index+=1

      end
    rescue
      puts "[Rescued]: array copy errors"
    end
    new_result
  end
end
